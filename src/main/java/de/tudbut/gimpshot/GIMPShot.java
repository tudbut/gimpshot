package de.tudbut.gimpshot;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import de.tudbut.gimpshot.clipboard.ImageTransferable;
import tudbut.tools.Tools2;

public class GIMPShot {

    public static void main(String[] args) throws IOException, AWTException, InterruptedException {
        // Screenshot immediately, dont wait for config.
        BufferedImage image = Tools2.screenshot();
        File f = new File(System.getenv().getOrDefault("TMPDIR", "/tmp") + "/gs_screenshot.png");
        f.deleteOnExit();
        FileOutputStream os = new FileOutputStream(f);
        ImageIO.write(image, "png", os);
        os.close();

        // Read config now
        Config config = new Config(getConfigPath());
        if(config.shouldCopy()) {
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new ImageTransferable(image, "GIMPShot Screenshot"), null);
            System.out.println("Copied!");
        }
        Runtime.getRuntime().exec(new String[] {
            "sh", 
            "-c", 
            config.getCommand()
                .replace("$f", f.getAbsolutePath())
                .replace("$r", image.getWidth() + "x" + image.getHeight())
                .replace("$R", image.getWidth() + "," + image.getHeight())
                .replace("$w", String.valueOf(image.getWidth()))
                .replace("$h", String.valueOf(image.getHeight()))
        }).waitFor();
    }

    public static String getConfigPath() {
        String cfg = System.getenv().getOrDefault("XDG_CONFIG_HOME", System.getProperty("user.home") + "/.config");
        new File(cfg, "gimpshot").mkdir();
        return new File(cfg + "/gimpshot", "config.tcn").getAbsolutePath();
    }
}
