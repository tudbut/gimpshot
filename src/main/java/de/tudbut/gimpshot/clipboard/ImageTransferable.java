package de.tudbut.gimpshot.clipboard;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;

public class ImageTransferable implements Transferable {

    Image imageData;
    String stringData;

    public ImageTransferable(Image imageData, String stringData) {
        this.imageData = imageData;
        this.stringData = stringData;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] {
            DataFlavor.stringFlavor,
            DataFlavor.imageFlavor
        };
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        DataFlavor[] flavors = getTransferDataFlavors();
        for(int i = 0; i < flavors.length ; i++) {
            if(flavors[i].equals(flavor))
                return true;
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if(flavor.equals(DataFlavor.stringFlavor))
            return stringData;
        if(flavor.equals(DataFlavor.imageFlavor))
            return imageData;
        throw new UnsupportedFlavorException(flavor);
    }

}
