package de.tudbut.gimpshot;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.swing.JOptionPane;

import de.tudbut.io.StreamReader;
import tudbut.parsing.TCN;

public class Config {
    TCN data;

    public Config(String path) {
        try {
            FileInputStream st = new FileInputStream(path);
            data = TCN.read(new StreamReader(st).readAllAsString());
            st.close();
        } catch(Exception e) {
            data = new TCN();
            data.set("command", "gimp -ndfs '$f'");
            data.set("copy", "false");
            try {
                FileOutputStream st = new FileOutputStream(path);
                st.write(data.toString().getBytes());
                st.flush();
                st.close();
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(null, "Unable to take screenshot because the config (" + path + ") could not be accessed (rw)");
                e.printStackTrace();
                e1.printStackTrace();
            }
        }
    }

    public String getCommand() {
        return data.getString("command");
    }

    public boolean shouldCopy() {
        Boolean copy = data.getBoolean("copy");
        if(copy == null) {
            copy = false;
        }
        return copy;
    }
}
