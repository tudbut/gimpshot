# GIMPShot

A screenshot tool which opens GIMP (or any other image editor) with a shot of the entire screen.

## Why java?

I originally wanted to use rust for this, however, screenshot-rs is completely broken and I don't 
feel like fixing it because I don't have much experience with rust.

## Roadmap

- Support for some primitive on-screen cutting
- Support for automatic copying to clipboard [DONE]
